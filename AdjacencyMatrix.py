import pdb
import numpy as np


class AdjacencyMatrix:

    @staticmethod
    def mapEdgeValue(valuesList, vertex, matrix):
        i = len(valuesList)
        for k in range(len(valuesList)):
            matrix[i][k] = valuesList[k]

    @staticmethod
    def build(array):
        size = array.pop(0)[0]
        matrix = np.zeros((size, size), dtype=int)
        for i in range(len(array)):
            AdjacencyMatrix.mapEdgeValue(array[i], i, matrix)
        return matrix


    @staticmethod
    def mapEdgeValueCustom(valuesList, vertex, matrix):
        i = len(valuesList)
        # import pdb; pdb.set_trace()
        for k in range(len(valuesList)):
            matrix[i][k] = valuesList[k]        #lower triangle
            matrix[k][i] = valuesList[k]        #upper triangle


    @staticmethod
    def buildMirroed(array):
        size = array.pop(0)[0]
        matrix = np.zeros((size, size), dtype=int)
        for i in range(len(array)):
            AdjacencyMatrix.mapEdgeValueCustom(array[i], i, matrix)
        return matrix

