import utils
from AdjacencyMatrix import AdjacencyMatrix
from Kruskal import Kruskal
from Prim import Prim
from Djikstra import Djikstra

data = utils.readFile('dji10Alt.txt')

#matrix = AdjacencyMatrix.buildMirroed(data)
#print(matrix)
size = data.pop(0)[0]
#Kruskal(data,size).applyKruskal()
Prim(data, size).applyPrim2()
# Djikstra(matrix).applyDjikstra()

