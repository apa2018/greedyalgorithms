import os
# from types import Vertex,Edge
import types


def castToInt(x):
    return int(x)


def tokenize(line):
    return [castToInt(x) for x in line.split()]


def readFile(path):
    """
    Reads a file located in the @path given as arg
    @path : path to file
    @return : 
    """

    directory = os.path.dirname(__file__)
    absolutePath = os.path.join(directory, path)
    with open("{}".format(absolutePath), 'r') as file:
        lines = [line.rstrip('\n') for line in file]
        array = [tokenize(x) for x in lines]
        return array


def findNonZeroes(matrix):
    edge = 0
    for i in range(matrix.shape[0]):
        for k in range(matrix.shape[1]):
            if matrix[i][k] != 0:
                edge += 1
    return edge
