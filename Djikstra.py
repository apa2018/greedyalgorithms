import sys
import numpy as np
from collections import OrderedDict


class Djikstra:
    def __init__(self, matrix, origin=0):
        self.matrix = matrix
        self.nbVertices = len(range(matrix.shape[0]))
        self.origin = origin
        self.queue = self.createQueue()
        self.pathCost = 0

    def createQueue(self):  # using a sorted dictionary
        dictQueue = {}
        for i in range(self.nbVertices):
            if i == self.origin:
              dictQueue[str(i)] = 0     
            else:
              dictQueue[str(i)] = sys.maxsize  # [0,1,2,...]->infinite
        
        return OrderedDict(sorted(dictQueue.items(), key=lambda t: t[1]))

    def updateQueue(self):
        self.queue = OrderedDict(sorted(self.queue.items(), key=lambda t: t[1]))

    def pickPath(self,vertex):
        self.pathCost += self.queue[str(vertex)]
        # newQueue = dict(self.queue)
        del self.queue[str(vertex)]

        self.updateQueue()
        # return next(iter(self.queue))

    def discoverNeighbors(self,vertex):
        for i in range(self.nbVertices):
          if (self.matrix[i][vertex] != 0) and (str(i) in self.queue):
            edge = self.queue[str(i)]
            if edge == sys.maxsize:                         #not yet discovered
              self.queue[str(i)] = self.matrix[i][vertex]
            else:
              self.queue[str(i)] += self.matrix[i][vertex]  #accumulate


    def applyDjikstra(self):
        # import pdb; pdb.set_trace()
        self.discoverNeighbors(self.origin)
        self.pickPath(self.origin)

        pondering = True if len(self.queue) > 0 else False        #"do I still need to calculate?"
        while pondering:
          nextInLine = int(next(iter(self.queue)))
          # print(self.queue)
          # print(nextInLine)
          import pdb; pdb.set_trace()
          self.discoverNeighbors(nextInLine)
          self.pickPath(nextInLine)
          if len(self.queue) < 1 or len(self.queue) == self.nbVertices:
            pondering = False
        
        print(self.pathCost)
