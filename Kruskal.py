import sys
import numpy as np
import utils


class Kruskal:
    def __init__(self, matrix, vertices):
        self.matrix = matrix
        self.nbVertices = vertices

    def getOrderedList(self):
        aux = self.matrix.copy()
        edges = []
        for i in range(len(aux)):
            for k in range(len(aux)):
                edgeValue = aux[i][k]
                if edgeValue != 0:
                    firstVertex, secondVertex, value = i, k, edgeValue
                    aux[i][k] = 0  # kind of removes
                    edges.append([firstVertex, secondVertex, value])
                    
        orderedEdges = sorted(edges, key=lambda x: x[2])
        return orderedEdges

    def find(self, parent, i):
        if parent[i] == i:
            return i
        return self.find(parent, parent[i])


    def union(self, parent, rank, x, y):
        xRoot = self.find(parent, x)
        yRoot = self.find(parent, y)

        # Attach smaller rank tree under root of
        # high rank tree (Union by Rank)
        if rank[xRoot] < rank[yRoot]:
            parent[xRoot] = yRoot
        elif rank[xRoot] > rank[yRoot]:
            parent[yRoot] = xRoot

        # If ranks are same, then make one as root
        # and increment its rank by one
        else:
            parent[yRoot] = xRoot
            rank[xRoot] += 1


    def applyKruskal(self):
        tup = self.getOrderedList()
        mst = np.zeros((len(self.matrix[0]), len(self.matrix[0])), dtype=int)
        parent = []
        rank = []
        e = 0 
        i = 0
        cost = 0
        selectedEdges = []
        

        for j in range(self.nbVertices):
            parent.append(j)
            rank.append(0)

        while e < self.nbVertices - 1:
            v1, v2, weigth = tup[i][0], tup[i][1], tup[i][2]
            i += 1

            x = self.find(parent, v1)
            y = self.find(parent, v2)
            
            if x != y:
                e += 1
                mst[v1][v2] = weigth
                self.union(parent, rank, x, y)
                cost+=weigth
                print('Selected Edges:',v1,'-',v2,'->',weigth)
                selectedEdges.append([v1,v2,weigth])
                
        print('Total cost:',cost)
        print('\n',mst)
        