import sys
import numpy as np


class Prim:
    def __init__(self, matrix, nbVertices):
      self.matrix = matrix
      self.nbVertices = nbVertices

    def buildFinalMST(self,finalMST,chosenEdges):
      cost = 0
      for i in chosenEdges:
          finalMST[i[0]][i[1]] = i[2]
          print(i)
          cost+=i[2]
      print(np.transpose(self.matrix))

    def findAdjacents(self, index, parent, key, mstSet):
      for i in range(len(self.matrix[0])):
        if self.matrix[index][i] > 0 and mstSet[i] == False and key[i] > self.matrix[index][i]:
          parent[i] = index
          key[i] = self.matrix[index][i]

    def findMin(self, key, mstSet):
      lowest = sys.maxsize
      min_index = sys.maxsize

      for i in range(self.nbVertices):
          current = key[i]
          if current < lowest and mstSet[i] == False:
              min_index, lowest = i, current

      return min_index


    def applyPrim2(self):
      #candidates = np.full((len(self.matrix[0]),len(self.matrix[1])),sys.maxsize,dtype=int)
      #mst = np.full((len(self.matrix[0]),len(self.matrix[1])),sys.maxsize,dtype=int)
      # chosenEdges = []
      key = [sys.maxsize] * self.nbVertices 
      parent = [None] * self.nbVertices
      mstSet = [False] * self.nbVertices
      cost = 0

      #mstSet[0] = True
      parent[0] = -1
      key[0]=0
      
      #self.findAdjacents(0, parent,key, mstSet)

      for i in range(self.nbVertices-1):
          newVertex = self.findMin(key,mstSet)
          mstSet[newVertex] = True
          cost+=key[newVertex]
          self.findAdjacents(newVertex, parent, key, mstSet)
      return cost
      # self.buildFinalMST(parent,chosenEdges)